<?php


class counter {

   protected $count;
   protected $filename;
   protected $RemoteIP;
   protected $DateTime;
   protected $Ref;
   protected $CurrentUrl;
   protected $Query;
   protected $full_url;
   protected $actual_url;
   protected $name;
   protected $value;
   
   function init()
   {
	  $this->filename="log.txt";
	  $this->RemoteIP=""; 
	  $this->DateTime=""; 
	  $this->Ref="";
	  $this->CurrentUrl="";
	  $this->Query="";
	  $this->aryNew =null;
	  $this->actual_url="";
	  $this->full_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
   }

	function start()
	{
		$this->SetCounter();
		$this->getIP();
		$this->getDate();
		$this->getRef();	
		$this->getCurrentUrl();
		$this->getQuery();
	}


  
 	function GetNewValue()
   {

	$this->aryNew[$this->count]["RemoteIP"]=$this->RemoteIP;
	$this->aryNew[$this->count]["DateTime"]=$this->DateTime;
	$this->aryNew[$this->count]["Ref"]=$this->Ref;
	$this->aryNew[$this->count]["CurrentUrl"]=$this->CurrentUrl;
	$this->aryNew[$this->count]["Query"]=$this->Query;
   } 
 
   
   function ReadArray_old()
   {
   
   if (file_exists($this->filename)) 
	{
	 	$my_arr = json_decode(file_get_contents($this->filename), true);
		if($my_arr===false)
		{
			echo "Opening error";
		}
		else
		{
			
	
			$len=count($my_arr);
			$i=0;
			echo "<table border='1'>";
				echo "<tr>";
					echo "<td>Counter</td>";
					echo "<td>IP</td>";
					echo "<td>DateTime</td>";
					echo "<td>Page</td>";
					echo "<td>Ref</td>";
					echo "<td>Query</td>";
				echo "</tr>";
				
			while($i<$len)
			{

				echo "<tr>";
				echo "<td>$i</td>";
				echo "<td>".$my_arr[$i]['RemoteIP']."</td>";
				echo "<td>".$my_arr[$i]['DateTime']."</td>";
				echo "<td>".$my_arr[$i]['CurrentUrl']."</td>";
				echo "<td>".$my_arr[$i]['Ref']."</td>";
				echo "<td>".$my_arr[$i]['Query']."</td>";
				echo "</tr>";
				$i++;
			}
			echo "</table>";
		}
	}
	else
	{
	echo "Missing log file";
	 } 	
   }
   
   function SetNameAndValue()
   {
   		
		if (!empty($_SERVER['QUERY_STRING']))
		{
		list($this->name,$this->value) = $this->custom_explode('=',$_SERVER['QUERY_STRING']); 

	   }
	   else
	   {
	   $this->name=null;
	   $this->value=null;
	   }
      
   }
   
 protected function custom_explode($findme_par,$mystring_par)
{

	$pos = strpos($mystring_par, $findme_par);
	$key = substr($mystring_par,0,$pos); 
	$value = substr($mystring_par, $pos+1); 
	$ary[0]=$key;
	$ary[1]=$value;
	return $ary;
	
}
   function ReadArray()
   {

	$this->SetNameAndValue();
   
   $this->actual_url=strtok($this->full_url,'?'); //rimuove la query string
   

   if($this->name!=null) //se non ho un filtro salto il blocco
   {
	   if($this->name!="DateTime")
	   {
	   	echo "<span><b>current filter</b>:".$this->name."=".$this->value."</span>";
	   }
	   else
	   {
	 	$this->value=$this->cleanDate($this->value);//rimuovo il %20
		$datePassed = new DateTime($this->value);
	  
	   echo "<span><b>current filter:</b>".$this->name."=".$datePassed->format('Y-m-d')."</span>";
	   }
	   
	   echo "<span><a href='".$this->actual_url ."'>"."<span id='remove'> remove this filter </span></a></span><br>";
   }
   

   if (file_exists("log.txt")) 
	{
	 	$my_arr = json_decode(file_get_contents("log.txt"), true);
		if($my_arr===false)
		{
			echo "Opening error";
		}
		else
		{

			$len=count($my_arr);
			$i=0;
			echo "<table border='1'>";
				echo "<tr>";
					echo "<td>Counter</td>";
					echo "<td>IP</td>";
					echo "<td>DateTime</td>";
					echo "<td>Page</td>";
					echo "<td>Ref</td>";
					echo "<td>Query</td>";
				echo "</tr>";
				$j=0;

										
			if ($this->value!==null)
			{

				while($i<$len)
				{

					if ($this->name!="DateTime")
					{
						if($my_arr[$i][$this->name]==$this->value)
						{
							$this->PrintRow($i,$j,$my_arr);
							$j++;
						}
					}
					else
					{
						$dateInFile = new DateTime($my_arr[$i][$this->name]);
						$this->value=$this->cleanDate($this->value);//rimuovo il %20
						$datePassed = new DateTime($this->value);
						

						
						if($dateInFile->format('Y-m-d')==$datePassed->format('Y-m-d'))
						
						{
							$this->PrintRow($i,$j,$my_arr);
							$j++;
						}
					}							
						$i++;
				}
				
				
			}	
			
			else
	
			{
				while($i<$len)
				{
					
						$this->PrintRow($i,$j,$my_arr);
						$j++;
						
						$i++;
				}			
			}
			
		}
	}
	else 
	{
	echo "Missing log file";
	 } 	
   }
	
	  	
    function WriteOrAppendArray()
   {
   	$this->GetNewValue();
   	if (file_exists($this->filename)) 
	{
		//recupero l'array su log.txt
	 	$ary_old_data = json_decode(file_get_contents($this->filename), true);
		if($ary_old_data===false)
		{
			
		}
		else
		{
			foreach ($this->aryNew as $v1=>$v2) 
			{

			    foreach ($v2 as $v3=>$v4)
				{
						$ary_old_data[$v1][$v3]=$v4; //forse da fare $aryStored[$v3]=$v4;
			    }				
		
			}
		}
		unlink($this->filename);
		file_put_contents($this->filename,  json_encode($ary_old_data,true));
	}
	else
	{
		$arr[$this->count]["RemoteIP"]=$this->RemoteIP;
		$arr[$this->count]["DateTime"]=$this->DateTime;
		$arr[$this->count]["Ref"]=$this->Ref;
		$arr[$this->count]["CurrentUrl"]=$this->CurrentUrl;
		$arr[$this->count]["Query"]=$this->Query;
		file_put_contents($this->filename,  json_encode($arr,true));
	}	
	}
	
   
   private function getIP()
   {

	if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
	{
   	 $this->RemoteIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else 
	{
     $this->RemoteIP = $_SERVER['REMOTE_ADDR'];
	}
	
   }
   
   private function getDate()
   {
   $this->DateTime=date("Y-m-d h:i:sa");
   }
    function increase()
   {
   	 $this->count++;

   }  
   
   private function getRef()
   {
   if(isset($_SERVER['HTTP_REFERER'])) {
  	$this->Ref=$_SERVER["HTTP_REFERER"];
   }
	else
	{
   	$this->Ref="";
	}

   }

   private function getCurrentUrl()
   {
  	$this->CurrentUrl=$_SERVER["REQUEST_URI"];
   }

    private function getQuery()
   {
  	$this->Query=parse_url($this->CurrentUrl, PHP_URL_QUERY);
   }
   
   private function SetCounter()
   {
   if (file_exists($this->filename)) 
	{
		
	 	$my_arr = json_decode(file_get_contents($this->filename), true);
		if($my_arr===false)
		{

			$this->count=0; //primo ingresso setto il contatore a 0
		}
		else
		{

			foreach ($my_arr as $v1=>$v2) 
			{

			}
				$v1++;
				$this->count=$v1;
		}
	}  	
   else
   		{

			$this->count=0; 
		}
   }  


protected function PrintRow($i,$j,$my_arr)
	{
	$datePassed = new DateTime($my_arr[$i]['DateTime']);
	
						echo "<tr>";
						echo "<td>".$j." "."</td>";
						
						echo "<td>"."<a href='".$this->actual_url."?RemoteIP=".$my_arr[$i]['RemoteIP'] ."'"."title='filter for=".$my_arr[$i]['RemoteIP']."'".">".$my_arr[$i]['RemoteIP']."</a>"."</td>";
						
						echo "<td>"."<a href='".$this->actual_url."?DateTime=".$my_arr[$i]['DateTime'] ."'"."title='filter for=".$datePassed->format('Y-m-d')."'".">".$my_arr[$i]['DateTime']."</a>"."</td>";	
							
						echo "<td>"."<a href='".$this->actual_url."?CurrentUrl=".$my_arr[$i]['CurrentUrl'] ."'"."title='filter for=".$my_arr[$i]['CurrentUrl']."'".">".$my_arr[$i]['CurrentUrl']."</a>"."</td>";	
									
						echo "<td>"."<a href='".$this->actual_url."?Ref=".$my_arr[$i]['Ref'] ."'"."title='filter for=".$my_arr[$i]['Ref']."'".">".$my_arr[$i]['Ref']."</a>"."</td>";	
								
						echo "<td>"."<a href='".$this->actual_url."?Query=".$my_arr[$i]['Query'] ."'"."title='filter for=".$my_arr[$i]['Query']."'".">".$my_arr[$i]['Query']."</a>"."</td>";
										
				
						echo "</tr>";	
	 }
	 
protected function cleanDate($textURL) {
  $URL = str_replace("%20"," ",$textURL);
            return $URL;
     }
}


?> 